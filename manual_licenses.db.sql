BEGIN TRANSACTION;
DROP TABLE IF EXISTS "manual_package_url";
CREATE TABLE IF NOT EXISTS "manual_package_url" (
	"Package_Group"	TEXT NOT NULL,
	"Package_Module"	TEXT NOT NULL,
	"URL"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "manual_package_license";
CREATE TABLE IF NOT EXISTS "manual_package_license" (
	"Package_Group"	TEXT NOT NULL,
	"Package_Module"	TEXT NOT NULL,
	"Custom_License_ID"	INTEGER NOT NULL,
	FOREIGN KEY("Custom_License_ID") REFERENCES "custom_license"("ID")
);
DROP TABLE IF EXISTS "generic_licenses";
CREATE TABLE IF NOT EXISTS "generic_licenses" (
	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"Generic_License_Name"	TEXT NOT NULL,
	"Generic_License_URL"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "generic_licenses_contents";
CREATE TABLE IF NOT EXISTS "generic_licenses_contents" (
	"Generic_License_ID"	INTEGER NOT NULL,
	"Content"	TEXT NOT NULL,
	FOREIGN KEY("Generic_License_ID") REFERENCES "generic_licenses"("ID")
);
DROP TABLE IF EXISTS "generic_license_aliases";
CREATE TABLE IF NOT EXISTS "generic_license_aliases" (
	"Generic_License_ID"	INTEGER NOT NULL,
	"Alias"	TEXT NOT NULL UNIQUE,
	FOREIGN KEY("Generic_License_ID") REFERENCES "generic_licenses"("ID")
);
DROP TABLE IF EXISTS "generic_licenses_regexes";
CREATE TABLE IF NOT EXISTS "generic_licenses_regexes" (
	"Regex"	TEXT NOT NULL UNIQUE,
	"Generic_License_ID"	INTEGER NOT NULL,
	FOREIGN KEY("Generic_License_ID") REFERENCES "generic_licenses"("ID")
);
DROP TABLE IF EXISTS "custom_license";
CREATE TABLE IF NOT EXISTS "custom_license" (
	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"Base_License"	INTEGER,
	"Custom_License_Name"	TEXT,
	"Custom_License_URL"	TEXT,
	"Custom_License_Content"	TEXT,
	"Custom_Notice_Content"	TEXT,
	"Custom_Notice_URL"	TEXT,
	FOREIGN KEY("Base_License") REFERENCES "generic_licenses"("ID")
);
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('com.squareup.retrofit2','retrofit','https://github.com/square/retrofit');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('com.squareup.retrofit2','converter-gson','https://github.com/square/retrofit/tree/master/retrofit-converters/gson');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('com.google.guava','guava','https://github.com/google/guava');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('io.jsonwebtoken','jjwt-api','https://github.com/jwtk/jjwt');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('io.jsonwebtoken','jjwt-impl','https://github.com/jwtk/jjwt');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('io.jsonwebtoken','jjwt-jackson','https://github.com/jwtk/jjwt');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('org.mapstruct','mapstruct','https://github.com/mapstruct/mapstruct');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('com.github.seratch','jslack','https://github.com/seratch/jslack');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('org.springframework.security.oauth','spring-security-oauth2','https://github.com/spring-projects/spring-security-oauth');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('com.google.code.gson','gson','https://github.com/google/gson');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('org.springframework.data','spring-data-commons','https://github.com/spring-projects/spring-data-commons');
INSERT INTO "manual_package_url" ("Package_Group","Package_Module","URL") VALUES ('org.mapstruct','mapstruct-processor','https://github.com/mapstruct/mapstruct');
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('org.slf4j','slf4j-api',1);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.squareup.retrofit2','retrofit',2);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.squareup.retrofit2','converter-gson',2);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.google.guava','guava',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.github.seratch','jslack',4);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.google.code.gson','gson',5);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('org.springframework.security.oauth','spring-security-oauth2',6);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('org.springframework.data','spring-data-commons',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('org.springframework.cloud','spring-cloud-starter-netflix-zuul',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.fasterxml.jackson.core','jackson-databind',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.fasterxml.jackson.core','jackson-core',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.fasterxml.jackson.core','jackson-annotations',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('com.fasterxml.jackson.dataformat','jackson-dataformat-xml',3);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('<NPM>','emotion-theming',7);
INSERT INTO "manual_package_license" ("Package_Group","Package_Module","Custom_License_ID") VALUES ('<NPM>','react-image-fallback',8);
INSERT INTO "generic_licenses" ("ID","Generic_License_Name","Generic_License_URL") VALUES (1,'MIT','https://opensource.org/licenses/MIT');
INSERT INTO "generic_licenses" ("ID","Generic_License_Name","Generic_License_URL") VALUES (2,'Apache 2.0','https://www.apache.org/licenses/LICENSE-2.0');
INSERT INTO "generic_licenses" ("ID","Generic_License_Name","Generic_License_URL") VALUES (3,'BSD 2-Clause "Simplified" License','https://opensource.org/licenses/BSD-2-Clause');
INSERT INTO "generic_licenses_contents" ("Generic_License_ID","Content") VALUES (1,'Copyright <YEAR> <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.');
INSERT INTO "generic_licenses_contents" ("Generic_License_ID","Content") VALUES (2,'
                                 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don''t include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
');
INSERT INTO "generic_licenses_contents" ("Generic_License_ID","Content") VALUES (3,'BSD 2-Clause License

Copyright (c) [year], [fullname]
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.');
INSERT INTO "generic_licenses_contents" ("Generic_License_ID","Content") VALUES (2,'
                                 Apache License
                           Version 2.0, January 2004
                        https://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don''t include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       https://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (2,'Apache 2.0');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (2,'Apache License, Version 2.0');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (2,'The Apache Software License, Version 2.0');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (2,'Apache License 2.0');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (1,'MIT');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (1,'MIT License');
INSERT INTO "generic_license_aliases" ("Generic_License_ID","Alias") VALUES (1,'The MIT License');
INSERT INTO "generic_licenses_regexes" ("Regex","Generic_License_ID") VALUES ('^.*\s*Apache License.*\s*Version 2.0.*$',2);
INSERT INTO "generic_licenses_regexes" ("Regex","Generic_License_ID") VALUES ('^.*Redistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions are met:\n\n1\. Redistributions of source code must retain the above copyright notice,\n   this list of conditions and the following disclaimer\.\n2\. Redistributions in binary form must reproduce the above copyright notice,\n   this list of conditions and the following disclaimer in the documentation\n   and/or other materials provided with the distribution\.\n\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"\nAND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE\nIMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE\nARE DISCLAIMED\. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE\nLIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR\nCONSEQUENTIAL DAMAGES \(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF\nSUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS\nINTERRUPTION\) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN\nCONTRACT, STRICT LIABILITY, OR TORT \(INCLUDING NEGLIGENCE OR OTHERWISE\)\nARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGE\.\n$',3);
INSERT INTO "generic_licenses_regexes" ("Regex","Generic_License_ID") VALUES ('^.*Permission is hereby granted, free of charge, to any person obtaining a copy.*to deal
in the Software without restriction.*The above copyright notice and this permission notice shall be included in all.*$',1);
INSERT INTO "generic_licenses_regexes" ("Regex","Generic_License_ID") VALUES ('^.*Apache \(Software\) License, version 2\.0.*$',2);
INSERT INTO "generic_licenses_regexes" ("Regex","Generic_License_ID") VALUES ('^(?:The )?MIT (?:LICENSE|License).*Permission is hereby granted, free of charge, to any person obtaining.*$',1);
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (1,1,NULL,'https://www.slf4j.org/license.html',' Copyright (c) 2004-2017 QOS.ch
 All rights reserved.

 Permission is hereby granted, free  of charge, to any person obtaining
 a  copy  of this  software  and  associated  documentation files  (the
 "Software"), to  deal in  the Software without  restriction, including
 without limitation  the rights to  use, copy, modify,  merge, publish,
 distribute,  sublicense, and/or sell  copies of  the Software,  and to
 permit persons to whom the Software  is furnished to do so, subject to
 the following conditions:
 
 The  above  copyright  notice  and  this permission  notice  shall  be
 included in all copies or substantial portions of the Software.
 
 THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.',NULL,NULL);
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (2,2,NULL,NULL,NULL,'Copyright 2013 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.','https://square.github.io/retrofit/#license');
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (3,2,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (4,1,NULL,'https://github.com/seratch/jslack/blob/master/LICENSE','(The MIT License)

Copyright (c) Kazuhiro Sera

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
''Software''), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED ''AS IS'', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


',NULL,NULL);
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (5,2,NULL,NULL,NULL,'Copyright 2008 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.','https://github.com/google/gson#license');
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (6,2,NULL,NULL,NULL,'======================================================================
== NOTICE file corresponding to section 4 d of the Apache License,  ==
== Version 2.0, in this case for the spring-security-oauth          ==
== distribution.                                                    ==
======================================================================

This product includes software developed by
the Apache Software Foundation (https://www.apache.org).

This product includes software developed by the Spring Framework
Project (https://www.springframework.org).

The end-user documentation included with a redistribution, if any,
must include the following acknowledgement:

  "This product includes software developed by Web Cohesion
   (https://www.webcohesion.com)."

Alternately, this acknowledgement may appear in the software itself,
if and wherever such third-party acknowledgements normally appear.

The name spring-security-oauth must not be used to endorse or promote
products derived from this software without prior written permission.
For written permission, please contact ryan@webcohesion.com.','https://github.com/spring-projects/spring-security-oauth/blob/master/notice.txt');
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (7,1,NULL,'https://github.com/emotion-js/emotion/blob/master/LICENSE','The MIT License (MIT)

Copyright (c) 2016 Kye Hohenberger

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
',NULL,NULL);
INSERT INTO "custom_license" ("ID","Base_License","Custom_License_Name","Custom_License_URL","Custom_License_Content","Custom_Notice_Content","Custom_Notice_URL") VALUES (8,2,NULL,NULL,NULL,'Copyright (C) 2017 Social Tables, Inc. (https://www.socialtables.com) All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.','https://github.com/socialtables/react-image-fallback');
COMMIT;
