import tarfile
import re
import logging
from zipfile import ZipFile
from io import BytesIO
from enum import Enum
import requests
import xmltodict
from flags import Flags

from Consts import Consts
from DB import DB
from npm_helper_methods import NpmHelperMethods


class LICENSE_SOURCE(Enum):
    '''
    An enum representing the source of the license.
    '''
    JAR = 0
    POM_LICENSES = 1
    POM_COMMENT = 2
    MANUAL = 3
    TGZ = 4
    NPM = 5


class InferredAttributes(Flags):
    '''
    A set of flags representing attributes that were inferred using other data, and were not given.
    '''
    license_content_from_url = ()
    notice_content_from_url = ()
    license_name_from_content = ()
    generic_license_name_from_alias = ()
    license_name_from_npm = ()


class License:
    '''
    A class representing a license object.
    '''
    __generic_licenses = set()

    def __hash__(self):
        return hash((self.license_content, self.notice_content, self.license_name, self.license_url, self.notice_url, self.source))

    def __eq__(self, other):
        if (self.license_content == other.license_content and
                self.notice_content == other.notice_content and
                self.license_name == other.license_name and
                self.license_url == other.license_url and
                self.notice_url == other.notice_url and
                self.source == other.source):
            return True
        return False

    def __init__(self, source: LICENSE_SOURCE, license_name: str = None, license_url: str = None, license_content: str = None, notice_url: str = None, notice_content: str = None):
        self.license_content = license_content
        self.notice_content = notice_content
        self.license_name = license_name
        self.license_url = license_url
        self.notice_url = notice_url
        self.source = source

        self.inferred_attributes = InferredAttributes.no_flags

        if not license_name and not license_url and not license_content:
            raise ValueError(
                'Must specify license name or license url or license content')

        if license_url and not license_content:
            self.license_content = License.__fetch_content_from_url(
                license_url)
            if self.license_content:
                self.inferred_attributes = self.inferred_attributes | InferredAttributes.license_content_from_url

        if notice_url and not notice_content:
            self.notice_content = License.__fetch_content_from_url(notice_url)
            if self.notice_content:
                self.inferred_attributes = self.inferred_attributes | InferredAttributes.notice_content_from_url

        if license_content and not license_name:
            self.license_name = License.generic_license_name_from_content(
                license_content)
            if self.license_name:
                self.inferred_attributes = self.inferred_attributes | InferredAttributes.license_name_from_content
            else:
                # no name was found
                self.license_name = None

        generic_license_name = License.get_license_name_from_alias(
            self.license_name)
        if generic_license_name != self.license_name:
            self.license_name = generic_license_name
            self.inferred_attributes = self.inferred_attributes | InferredAttributes.generic_license_name_from_alias

        self.is_exact_generic_license = License.__is_license_content_generic(
            self.license_content)

        if self.is_exact_generic_license and not self.license_url:
            self.license_url = License.__get_generic_license_url(
                self.license_name)

    @staticmethod
    def __is_license_content_generic(license_content: str) -> bool:

        # Fill static cache
        if len(License.__generic_licenses) == 0:
            generic_licenses = DB.execute_query(
                '''SELECT Content FROM generic_licenses_contents''')
            License.__generic_licenses = set(
                map(lambda x: tuple(x['Content'].split()), generic_licenses))

        if not license_content:
            return False

        if tuple(license_content.split()) in License.__generic_licenses:
            return True
        else:
            return False

    @staticmethod
    def __fetch_content_from_url(url: str):
        response_obj = requests.get(url)
        response_text = response_obj.text
        if 'text/plain' in response_obj.headers['content-type']:
            return response_text
        else:
            return None

    @staticmethod
    def get_gradle_license(group: str, module: str, version: str):
        jar_license = License.__license_from_jar_multiple_repos(
            group, module, version)
        if not jar_license:
            pom_license = License.__license_from_pom_multiple_repos(
                group, module, version)
            if not pom_license:
                manual_license = License.__license_from_manual(
                    group, module)
                if not manual_license:
                    logging.info('No License: %s, %s, %s',
                                 group, module, version)
                    return None
                else:
                    return manual_license
            else:
                return pom_license
        else:
            return jar_license

    @staticmethod
    def __license_from_jar_multiple_repos(group: str, module: str, version: str):
        for base_repo in Consts.BASE_REPO_URLS:
            group_parts = group.split('.')

            relative_group_url = '{0}/{1}/{2}/'.format(
                '/'.join(group_parts), module, version)
            full_url = base_repo + relative_group_url

            if requests.get(full_url).status_code == 200:
                return License.__license_from_jar(full_url, module, version)

        return None

    @staticmethod
    def __license_from_pom_multiple_repos(group: str, module: str, version: str):
        for base_repo in Consts.BASE_REPO_URLS:
            group_parts = group.split('.')

            relative_group_url = '{0}/{1}/{2}/'.format(
                '/'.join(group_parts), module, version)
            full_url = base_repo + relative_group_url

            if requests.get(full_url).status_code == 200:
                return License.__license_from_pom(full_url, module, version)

        return None

    @staticmethod
    def __license_from_jar(maven_repository_url: str, module: str, version: str):
        jar_name = '{0}-{1}.jar'.format(module, version)
        jar_content = requests.get(maven_repository_url + jar_name).content
        jar_file_obj = BytesIO(jar_content)
        jar_file_zip_obj = ZipFile(jar_file_obj)

        license_from_jar = License.extract_file_content_from_jar(
            jar_file_zip_obj, ['META-INF/LICENSE', 'META-INF/LICENSE.txt', 'META-INF/license.txt'])

        if not license_from_jar:
            return None

        notice_from_jar = License.extract_file_content_from_jar(
            jar_file_zip_obj, ['META-INF/NOTICE', 'META-INF/NOTICE.txt', 'META-INF/notice.txt'])

        return License(license_content=license_from_jar, notice_content=notice_from_jar, source=LICENSE_SOURCE.JAR)

    @staticmethod
    def extract_file_content_from_jar(jar_file_zip_obj: ZipFile, possible_names: list):
        files_in_zip = jar_file_zip_obj.namelist()
        content = ''
        for possible_name in possible_names:
            if possible_name in files_in_zip:
                try: 
                    content = jar_file_zip_obj.read(possible_name).decode('utf-8')
                except:
                    content = jar_file_zip_obj.read(possible_name).decode('latin-1')
            return content

        return None

    @staticmethod
    def __license_from_pom(maven_repository_url: str, module: str, version: str):
        pom_filename = '{0}-{1}.pom'.format(module, version)
        pom_content = requests.get(maven_repository_url + pom_filename).content

        parsed_pom = xmltodict.parse(pom_content, process_comments=True)

        if 'licenses' in parsed_pom['project']:
            license_part = parsed_pom['project']['licenses']['license']

            if 'name' in license_part:
                license_name = license_part['name']
            else:
                return None

            license_url = license_part['url'] if 'url' in license_part else None

            return License(license_name=license_name, license_url=license_url, source=LICENSE_SOURCE.POM_LICENSES)

        elif '#comment' in parsed_pom:

            # if the comment contains the word 'license', then assuming it is the license
            if 'license' in parsed_pom['#comment'].lower():
                license_content = parsed_pom['#comment']
                return License(license_content=license_content, source=LICENSE_SOURCE.POM_COMMENT)

        else:
            return None

    @staticmethod
    def __license_from_manual(group: str, module: str):
        custom_license_id_query_result = DB.execute_query('''SELECT Custom_License_ID
                            FROM   manual_package_license
                            WHERE  package_group = ?
                                   AND package_module = ?''', (group, module))

        if len(custom_license_id_query_result) > 1:
            raise ValueError('There are multiple rows in the DB for group {0}, module {1}'.format(
                group, module))

        if len(custom_license_id_query_result) == 0:
            return None

        custom_license_id = custom_license_id_query_result[0]['Custom_License_ID']

        generic_license_id = DB.execute_query(
            '''SELECT Base_License FROM custom_license WHERE ID = ?''', (custom_license_id,))[0]['Base_License']
        custom_license_attributes = DB.execute_query('''SELECT custom_license.custom_license_name,
       custom_license.custom_license_url,
       custom_license.custom_license_content,
       custom_license.custom_notice_url,
       custom_license.custom_notice_content
       FROM custom_license
       WHERE ID = ?''', (custom_license_id,))[0]

        generic_license_attributes = DB.execute_query('''SELECT generic_licenses.generic_license_name,
       generic_licenses.generic_license_url
       FROM generic_licenses
       WHERE generic_licenses.ID = ?''', (generic_license_id,))[0]

        generic_license_content = DB.execute_query(
            '''SELECT Content FROM generic_licenses_contents WHERE Generic_License_ID = ? LIMIT 1''', (generic_license_id,))[0]['Content']

        license_name = custom_license_attributes['Custom_License_Name'] if custom_license_attributes[
            'Custom_License_Name'] else generic_license_attributes['Generic_License_Name']

        license_url = custom_license_attributes['Custom_License_URL'] if custom_license_attributes[
            'Custom_License_URL'] else generic_license_attributes['Generic_License_URL']

        license_content = custom_license_attributes['Custom_License_Content'] if custom_license_attributes[
            'Custom_License_Content'] else generic_license_content

        notice_url = custom_license_attributes['Custom_Notice_URL']
        notice_content = custom_license_attributes['Custom_Notice_Content']

        return License(license_name=license_name, license_url=license_url, license_content=license_content, notice_url=notice_url, notice_content=notice_content, source=LICENSE_SOURCE.MANUAL)

    @staticmethod
    def generic_license_name_from_content(license_content):
        generic_regexes = DB.execute_query(
            '''SELECT * FROM generic_licenses_regexes''')
        if len(generic_regexes) == 0:
            raise ValueError('There are no regexes in the db')

        for result_row in generic_regexes:
            regex = result_row['Regex']
            if re.match(regex, license_content, flags=re.DOTALL):
                corresponding_generic_license_id = result_row['Generic_License_ID']
                generic_licenses = DB.execute_query(
                    '''SELECT Generic_License_Name FROM generic_licenses WHERE ID = ?''', (corresponding_generic_license_id,))

                if len(generic_licenses) == 0:
                    raise ValueError('There is no generic license with ID {0}'.format(
                        corresponding_generic_license_id))
                elif len(generic_licenses) > 1:
                    raise ValueError('There is more than 1 generic license with ID {0}'.format(
                        corresponding_generic_license_id))

                corresponding_generic_license = generic_licenses[0]
                return corresponding_generic_license['Generic_License_Name']

        return None

    @staticmethod
    def get_license_name_from_alias(alias: str):
        sqlite_results = DB.execute_query(
            '''SELECT Generic_License_Name FROM generic_licenses WHERE ID = (SELECT Generic_License_ID FROM generic_license_aliases WHERE Alias = ?)''', (alias,))
        if len(sqlite_results) == 0:
            # raise ValueError('Alias {0} was not found'.format(alias))
            return alias
        if len(sqlite_results) > 1:
            raise ValueError(
                'There are multiple generic licenses that are associated with alias {0} (Probably a DB error)'.format(alias))

        sqlite_row = sqlite_results[0]
        return sqlite_row['Generic_License_Name']

    @staticmethod
    def __get_generic_license_url(license_name: str) -> str:
        sqlite_results = DB.execute_query(
            '''SELECT Generic_License_URL FROM generic_licenses WHERE Generic_License_Name = ?''', (license_name,))
        if len(sqlite_results) == 0:
            return None
        if len(sqlite_results) > 1:
            raise ValueError(
                'There are multiple generic licenses with the name "{0}" (Probably a DB error)'.format(license_name))

        sqlite_row = sqlite_results[0]
        return sqlite_row['Generic_License_URL']

    @staticmethod
    def __license_from_tgz_path(resolved_filepath: str):
        tgz_request = requests.get(resolved_filepath)
        if tgz_request.status_code == 200:
            tgz_content = tgz_request.content
            tgz_file_obj = BytesIO(tgz_content)
            tar_file_obj = tarfile.open(fileobj=tgz_file_obj, mode='r:gz')

            license_from_tgz = License.__extract_file_content_from_tar(
                tar_file_obj, ['package/LICENSE', 'package/license', 'package/LICENSE.txt', 'package/license.txt', 'package/LICENSE.md', 'package/license.md'])

            if not license_from_tgz:
                return None

            notice_from_tgz = License.__extract_file_content_from_tar(
                tar_file_obj, ['package/NOTICE'])

            return License(license_content=license_from_tgz, notice_content=notice_from_tgz, source=LICENSE_SOURCE.TGZ)

        return None

    @staticmethod
    def __extract_file_content_from_tar(tar_file_obj: tarfile.TarFile, possible_names: list) -> str:
        files_in_tar = tar_file_obj.getnames()
        for possible_name in possible_names:
            if possible_name in files_in_tar:
                with tar_file_obj.extractfile(possible_name) as extracted_file_handler:
                    content = extracted_file_handler.read().decode('utf-8')
                return content

        return None

    @staticmethod
    def get_npm_license(package_name: str, version: str, resolved_filepath: str):
        tgz_license = License.__license_from_tgz_path(resolved_filepath)
        if tgz_license:
            if tgz_license.license_name:
                return tgz_license
            else:
                json_obj = NpmHelperMethods.get_npm_info_obj(
                    package_name, version)
                if json_obj and 'license' in json_obj:
                    license_name = json_obj['license']
                    tgz_license.license_name = license_name
                    tgz_license.inferred_attributes = tgz_license.inferred_attributes | InferredAttributes.license_name_from_npm
                return tgz_license
        else:
            manual_license = License.__license_from_manual(
                '<NPM>', package_name)
            if manual_license:
                return manual_license
            else:
                json_obj = NpmHelperMethods.get_npm_info_obj(
                    package_name, version)
                if json_obj and 'license' in json_obj:
                    license_name = json_obj['license']
                    license_content = json_obj['licenseText'] if 'licenseText' in json_obj else None
                    license_obj = License(source=LICENSE_SOURCE.NPM,
                                          license_name=license_name, license_content=license_content)
                    license_obj.inferred_attributes = license_obj.inferred_attributes | InferredAttributes.license_name_from_npm
                    return license_obj

        return None
