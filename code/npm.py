import os
import logging
import json


from NpmDependency import NpmDependency


class NpmHandler:
    def __init__(self, root_dir: str):
        package_json_files = NpmHandler.__get_all_package_json_files(
            root_dir, True)

        shrinkwrap_content = NpmHandler.__get_all_shrinkwrap_content(root_dir)

        self.all_dependencies = set()
        self.dependencies_for_pack = {}

        for package_json_file in package_json_files:
            direct_dep_names = NpmHandler.__get_all_direct_deps_names(
                package_json_file)

            direct_deps_from_shrinkwrap = NpmHandler.__filter_shrinkwrap_direct_deps(
                direct_dep_names, shrinkwrap_content)

            component_name = NpmHandler.__component_name_from_path(
                root_dir, package_json_file)
            self.dependencies_for_pack[component_name] = []

            for dep_name in direct_deps_from_shrinkwrap:
                if dep_name.startswith('@nsknox'):
                    continue

                version = direct_deps_from_shrinkwrap[dep_name]['version']
                resolved_filepath = direct_deps_from_shrinkwrap[dep_name]['resolved']

                dep_obj = NpmDependency(dep_name, version, resolved_filepath)

                self.all_dependencies.add(dep_obj)
                self.dependencies_for_pack[component_name].append(dep_obj)

    @staticmethod
    def __get_all_package_json_files(root_dir: str, ignore_node_modules: bool = True) -> list:
        logging.debug('started: __get_all_package_json_files')
        to_ret = []
        for path, _, files in os.walk(root_dir):
            for name in files:
                if name == 'package.json':
                    if ignore_node_modules and 'node_modules' in path:
                        continue
                    else:
                        to_ret.append(os.path.join(path, name))

        logging.debug('finished: __get_all_package_json_files')
        return to_ret

    @staticmethod
    def __get_all_shrinkwrap_content(root_dir: str) -> dict:
        shrinkwrap_path = os.path.join(root_dir, 'npm-shrinkwrap.json')
        if os.path.isfile(shrinkwrap_path):
            with open(shrinkwrap_path, 'r') as file_handler:
                json_content = json.load(file_handler)
                return json_content
        else:
            raise ValueError(
                'Directory "{0}" does not containt a file named "npm-shrinkwrap.json"'.format(root_dir))

    @staticmethod
    def __get_all_direct_deps_names(package_json_file: str) -> set:
        with open(package_json_file, 'r') as file_handler:
            json_content = json.load(file_handler)

            if 'dependencies' in json_content:
                dep_names = set(json_content['dependencies'].keys())
            else:
                dep_names = set()

            return dep_names

    @staticmethod
    def __filter_shrinkwrap_direct_deps(direct_deps: set, shrinkwrap_content: dict) -> dict:
        to_ret = {}
        for dep_name in shrinkwrap_content['dependencies']:
            if dep_name in direct_deps:
                to_ret[dep_name] = shrinkwrap_content['dependencies'][dep_name]

        return to_ret

    @staticmethod
    def __component_name_from_path(root_dir: str, path: str):
        logging.debug('started: __component_name_from_path')
        if not path.startswith(root_dir):
            raise ValueError(
                'Component {0} is not located inside root directory {1}'.format(path, root_dir))

        if not path.endswith('\\package.json'):
            raise ValueError(
                'Component {0} is not a path for build.gradle file'.format(path))

        build_gradle_dir = os.path.dirname(path)
        relative_path = os.path.relpath(build_gradle_dir, root_dir)

        logging.debug('finished: __component_name_from_path')
        return relative_path.strip('\\')
