from Dependency import Dependency
from License import License
from npm_helper_methods import NpmHelperMethods


class NpmDependency(Dependency):
    def __hash__(self):
        # version is not calculated deliberately
        return hash((self.package_name, hash(self.license)))

    def __eq__(self, other):
        # version is not compared deliberately
        if self.package_name == other.package_name and self.license == other.license:
            return True
        return False

    def __init__(self, package_name: str, version: str, resolved_filepath: str):
        self.package_name = package_name
        self.resolved_filepath = resolved_filepath

        dep_url = NpmDependency.__url_from_npm(package_name, version)
        dep_license = License.get_npm_license(
            package_name, version, resolved_filepath)
        super().__init__(version=version, url=dep_url,
                         display_name=package_name, license_obj=dep_license)

    @staticmethod
    def __url_from_npm(package: str, version: str) -> str:
        json_obj = NpmHelperMethods.get_npm_info_obj(package, version)
        if 'homepage' in json_obj:
            return json_obj['homepage']
        elif 'repository' in json_obj and 'url' in json_obj['repository'] and json_obj['repository']['url'].startswith('http'):
            return json_obj['repository']['url']
        return None
