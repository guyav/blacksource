import sqlite3


class DB:
    __conn = sqlite3.connect('file:manual_licenses.db?mode=ro', uri=True)
    __conn.row_factory = sqlite3.Row
    __c = __conn.cursor()

    @staticmethod
    def execute_query(query: str, params: tuple = ()) -> list:
        DB.__c.execute(query, params)
        return DB.__c.fetchall()
