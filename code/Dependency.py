from abc import ABC
from License import License


class Dependency(ABC):
    def __init__(self, version: str, url: str, display_name: str, license_obj: License):
        self._version = version
        self._url = url
        self._display_name = display_name
        self._license = license_obj

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, value):
        self._url = value

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    @property
    def display_name(self):
        return self._display_name

    @property
    def license(self):
        return self._license

    @license.setter
    def license(self, value):
        self._license = value
