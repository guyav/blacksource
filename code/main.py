import argparse
import logging
import os
import csv
from enum import Enum
from yattag import Doc, indent


class PATH_TYPE(Enum):
    GRADLE = 0
    NPM = 1


def detect_path_type(path: str) -> PATH_TYPE:
    if os.path.isfile(os.path.join(path, 'gradlew')):
        return PATH_TYPE.GRADLE

    if os.path.isfile(os.path.join(path, 'package.json')):
        return PATH_TYPE.NPM

    raise ValueError('Path "{0}" is not of any known type'.format(path))


def main():
    parser = argparse.ArgumentParser(
        description='Get direct dependencies licenses')
    parser.add_argument(
        'path', type=str, help='root project directory. Optionally, specify more root project directories.', nargs='+')
    parser.add_argument('--internal', nargs='?', type=str, const='internal.csv',
                        help='generate internal report. Optionally, specifiy a path for the file (the default is "internal.csv").')
    parser.add_argument('--external', nargs='?', type=str, const='external.html',
                        help='generate external report. Optionally, specifiy a path for the file (the default is "external.html").')
    parser.add_argument('--log-level', type=str, help='log level', default='INFO',
                        choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'NOTEST'])

    args = parser.parse_args()

    if not args.internal and not args.external:
        logging.critical(
            'No report type (internal / external) is specified.')
        return

    logging.basicConfig(level=args.log_level)

    organised_deps = {}
    all_dependencies = set()

    for root_dir in args.path:
        root_path_type = detect_path_type(root_dir)

        if root_path_type == PATH_TYPE.GRADLE:
            from gradle import GradleHandler
            handler = GradleHandler(root_dir)
        elif root_path_type == PATH_TYPE.NPM:
            from npm import NpmHandler
            handler = NpmHandler(root_dir)
        else:
            raise ValueError('Unknown PATH_TYPE: {0}'.format(root_path_type))

        all_dependencies.update(handler.all_dependencies)
        organised_deps[root_dir] = handler.dependencies_for_pack

    if args.internal:
        logging.info('started generating internal report')
        internal_report_path = args.internal
        generate_internal_report(
            organised_deps, internal_report_path)
        logging.info('finished generating internal report')

    if args.external:
        logging.info('started generating external report')
        external_report_path = args.external
        generate_external_report(
            all_dependencies, external_report_path)
        logging.info('finished generating external report')


def generate_internal_report(organised_deps: dict, report_path: str):
    # {'root_dir': {'component': [dependency,dependecy,...]}}
    logging.debug('started: generate_internal_report')
    with open(report_path, 'w', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['Root Dir', 'Component Type', 'Component', 'Full Name', 'Version',
                         'License Name', 'Source', 'Inferred Attributes'])
        for root_dir in organised_deps:
            dependencies_dict = organised_deps[root_dir]
            for component_name in dependencies_dict:
                for dependency in dependencies_dict[component_name]:
                    if dependency.license:
                        writer.writerow(
                            [root_dir, type(dependency).__name__, component_name, dependency.display_name, dependency.version,
                             dependency.license.license_name, dependency.license.source,
                             dependency.license.inferred_attributes])
                    else:
                        logging.debug('dependency %s of component %s has no license',
                                      dependency.display_name, component_name)
                        writer.writerow(
                            [root_dir, type(dependency).__name__, component_name, dependency.display_name, dependency.version, 'No License Name'])
    logging.debug('finished: generate_internal_report')


def generate_external_report(dependencies_set: set, report_path: str):
    logging.debug('started: generate_external_report')
    doc, tag, text = Doc().tagtext()
    doc.asis('<!doctype html>')
    with tag('html', ('lang', 'en')):
        with tag('head'):
            doc.stag('meta', ('charset', 'utf-8'))
            doc.asis(
                '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">')
            doc.asis('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">')
            with tag('title'):
                text('Dependencies Report')
        with tag('body'):
            with tag('table', ('class', 'table table-hover')):
                with tag('thead'):
                    with tag('tr'):
                        with tag('th', ('scope', 'col')):
                            text('Dependency Name')
                        with tag('th', ('scope', 'col')):
                            text('URL')
                        with tag('th', ('scope', 'col')):
                            text('License Name')
                        with tag('th', ('scope', 'col')):
                            text('License Content')
                        with tag('th', ('scope', 'col')):
                            text('Notice Content')
                with tag('tbody'):
                    for dep in dependencies_set:
                        name = dep.display_name
                        dependency_url = dep.url if dep.url else None
                        license_name = dep.license.license_name if dep.license and dep.license.license_name else 'No license name'
                        license_content = dep.license.license_content if dep.license and dep.license.license_content else None
                        notice_content = dep.license.notice_content if dep.license and dep.license.notice_content else 'No notice'

                        with tag('tr'):
                            with tag('td'):
                                text(name)
                            with tag('td'):
                                if dependency_url:
                                    with tag('a', ('href', dependency_url)):
                                        text(dependency_url)
                                else:
                                    text('No URL')
                            with tag('td'):
                                text(license_name)

                            with tag('td'):
                                if license_content:
                                    if dep.license.is_exact_generic_license:
                                        with tag('a', ('href', dep.license.license_url)):
                                            text('Generic License')
                                    else:
                                        with tag('span', ('style', 'white-space:pre')):
                                            text(license_content)
                                else:
                                    text('No license content available')

                            with tag('td'):
                                text(notice_content)

    with open(report_path, 'w', newline='', encoding='utf-8') as f:
        f.write(indent(doc.getvalue(), indent_text=True))

    logging.debug('finished: generate_external_report')


if __name__ == "__main__":
    main()
