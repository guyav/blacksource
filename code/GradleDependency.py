import logging
import requests
import xmltodict
from License import License
from Consts import Consts
from DB import DB
from Dependency import Dependency


class GradleDependency(Dependency):
    def __hash__(self):
        # version is not calculated deliberately
        return hash((self.group, self.module, self.url, hash(self.license)))

    def __eq__(self, other):
        # version is not compared deliberately
        if self.group == other.group and self.module == other.module and self.license == other.license and self.url == other.url:
            return True
        return False

    def __init__(self, full_name: str, version: str):
        (self.group, self.module) = full_name.split(':')

        url = GradleDependency.__get_url_in_all_methods(
            self.group, self.module, version)
        license_obj = License.get_gradle_license(
            self.group, self.module, version)
        super().__init__(version, url, full_name, license_obj)

    @staticmethod
    def __url_from_pom(group: str, module: str, version: str) -> str:
        group_url_format = '/'.join(group.split('.'))
        for base_repo_url in Consts.BASE_REPO_URLS:
            pom_url = '{0}{1}/{2}/{3}/{2}-{3}.pom'.format(
                base_repo_url, group_url_format, module, version)
            response = requests.get(pom_url)
            if response.status_code == 200:
                xml_content = response.content
                parsed_pom = xmltodict.parse(xml_content)
                if 'project' in parsed_pom and 'url' in parsed_pom['project']:
                    url = parsed_pom['project']['url']
                    logging.info(
                        'POM file for %s, %s, %s contains URL %s', group, module, version, url)
                    return url
                else:
                    logging.info(
                        'POM file for %s, %s, %s does not contain "URL" key', group, module, version)
            else:
                logging.info('no POM file for %s, %s, %s in repo %s',
                             group, module, version, base_repo_url)

        logging.info('no POM file for %s, %s, %s in all repos',
                     group, module, version)
        return None

    @staticmethod
    def __url_from_manual(group: str, module: str) -> str:
        sqlite_results = DB.execute_query(
            '''SELECT URL from manual_package_url WHERE Package_Group = ? AND Package_Module = ?''', (group, module))
        if len(sqlite_results) == 0:
            logging.debug('No manual URL in DB for %s, %s',
                          group, module)
            return None
        if len(sqlite_results) > 1:
            logging.debug(
                'There are multiple URLs in the DB for %s, %s (Probably a DB error)', group, module)
            return None

        url = sqlite_results[0]['URL']
        logging.debug('Found manual URL in DB for %s, %s: %s',
                      group, module, url)
        return url

    @staticmethod
    def __get_url_in_all_methods(group: str, module: str, version: str) -> str:
        logging.debug('Trying to get URL from POM for %s, %s, %s',
                      group, module, version)
        url_from_pom = GradleDependency.__url_from_pom(group, module, version)
        if url_from_pom:
            logging.debug('URL from POM for %s, %s, %s was found: %s',
                          group, module, version, url_from_pom)
            return url_from_pom

        logging.debug('Trying to get URL from DB for %s, %s',
                      group, module)
        url_from_manual = GradleDependency.__url_from_manual(
            group, module)
        if url_from_manual:
            logging.debug('URL from DB for %s, %s was found: %s',
                          group, module, url_from_manual)
            return url_from_manual

        logging.info('URL was not found in any method for %s, %s, %s',
                     group, module, version)
        return None
