import re
import logging
import os

from GradleDependency import GradleDependency


class GradleHandler:
    dependencies_cache = {}

    def __init__(self, root_dir: str):
        versions_variables = GradleHandler.__get_versions_dict(root_dir)
        build_gradle_paths = GradleHandler.__get_all_build_gradle_files(
            root_dir)

        self.all_dependencies = set()
        self.dependencies_for_pack = {}

        for build_gradle_path in build_gradle_paths:
            logging.info(
                'started processing file %s', build_gradle_path)
            dependencies = GradleHandler.__get_dependencies_for_build_gradle_file(
                build_gradle_path, versions_variables)

            logging.debug(
                'finished getting dependencies for file %s', build_gradle_path)

            self.all_dependencies.update(dependencies)
            self.dependencies_for_pack[GradleHandler.__component_name_from_path(
                root_dir, build_gradle_path)] = dependencies
            logging.info(
                'finished processing file %s', build_gradle_path)

    @staticmethod
    def __get_all_build_gradle_files(root_dir: str):
        logging.debug('started: __get_all_build_gradle_files')
        to_ret = []
        for path, _, files in os.walk(root_dir):
            for name in files:
                if name == 'build.gradle':
                    to_ret.append(os.path.join(path, name))

        logging.debug('finished: __get_all_build_gradle_files')
        return to_ret

    @staticmethod
    def __get_versions_dict(root_dir: str):
        logging.debug('started: get_versions_dict')
        versions_dict = {}
        with open(os.path.join(root_dir, 'versions.gradle'), 'r') as handler:
            logging.debug('opened versions.gradle file')
            lines = handler.readlines()

        for line in lines:
            match_obj = re.match(r'^\s*(.*Version) = "(.*)"( \/\/(.*))*$', line)
            if match_obj:
                name = match_obj.group(1)
                version = match_obj.group(2)

                if name in versions_dict:
                    logging.error(
                        'Versions file contains multiple instances of %s', name)
                    raise ValueError(
                        'Versions file contains multiple instances of {0}'.format(name))

                else:
                    logging.debug(
                        'found version %s for variable %s', version, name)
                    versions_dict[name] = version

        logging.debug('finished: get_versions_dict')
        return versions_dict

    @staticmethod
    def __get_dependencies_for_build_gradle_file(build_file_path: str, versions_dict: dict):
        logging.debug('started: get_dependencies_for_build_gradle_file')
        to_ret = []
        with open(build_file_path, 'r') as handler:
            lines = handler.readlines()

        for line in lines:
            match_obj_impl = re.match(
                r'^\s*implementation "(.*):\$(.*Version)"\n?$', line)
            match_obj_compile = re.match(
                r'^\s*compile "(.*):\$(.*Version)"\n?$', line)
            if match_obj_impl or match_obj_compile:
                logging.debug('found dependency line: <%s>', line)
                if match_obj_impl:
                    dependency_name = match_obj_impl.group(1)
                    dependency_version_variable_name = match_obj_impl.group(2)
                    logging.debug('found implementation dependency line: <%s>. name: %s, version variable: %s',
                                  line, dependency_name, dependency_version_variable_name)
                if match_obj_compile:
                    dependency_name = match_obj_compile.group(1)
                    dependency_version_variable_name = match_obj_compile.group(
                        2)
                    logging.debug('found compile dependency line: <%s>. name: %s, version variable: %s',
                                  line, dependency_name, dependency_version_variable_name)

                if dependency_name.startswith('nsKnox.knoxSDK'):
                    logging.debug(
                        'dependency line ignored: <%s>', line)
                    continue

                if dependency_version_variable_name in versions_dict:
                    logging.debug(
                        'dependency version variable name exists in versions.gradle file')
                    dependency_version = versions_dict[dependency_version_variable_name]
                else:
                    logging.error(
                        'dependency version variable name does not exist in versions.gradle file')
                    raise ValueError('Cannot find version variable named {0} for {1} in versions file'.format(
                        dependency_version_variable_name, dependency_name))

                if (dependency_name, dependency_version) not in GradleHandler.dependencies_cache:
                    logging.debug('dependency <%s> with version <%s> was not encountered previously',
                                  dependency_name, dependency_version)
                    found_dependency = GradleDependency(
                        dependency_name, dependency_version)
                    GradleHandler.dependencies_cache[(
                        dependency_name, dependency_version)] = found_dependency
                    logging.debug('dependency <%s> with version <%s> was added to cache',
                                  dependency_name, dependency_version)

                logging.debug(
                    'dependency <{0}> with version <{1}> is in the cache, so using the cache')
                found_dependency = GradleHandler.dependencies_cache[(
                    dependency_name, dependency_version)]
                to_ret.append(found_dependency)

        logging.debug('finished: get_dependencies_for_build_gradle_file')
        return to_ret

    @staticmethod
    def __component_name_from_path(root_dir: str, path: str):
        logging.debug('started: component_name_from_path')
        if not path.startswith(root_dir):
            raise ValueError(
                'Component {0} is not located inside root directory {1}'.format(path, root_dir))

        if not path.endswith('/build.gradle'):
            raise ValueError(
                'Component {0} is not a path for build.gradle file'.format(path))

        build_gradle_dir = os.path.dirname(path)
        relative_path = os.path.relpath(build_gradle_dir, root_dir)

        logging.debug('finished: component_name_from_path')
        return relative_path.strip('/')
