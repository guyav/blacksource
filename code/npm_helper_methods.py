import subprocess
import json


class NpmHelperMethods():
    @staticmethod
    def get_npm_info_obj(package: str, version: str) -> dict:
        result = subprocess.run(['npm', 'view', '{0}@{1}'.format(
            package, version), '--json'], stdout=subprocess.PIPE, shell=True)
        if result.returncode == 0:
            json_obj = json.loads(result.stdout)
            return json_obj
        else:
            return None
